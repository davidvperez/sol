//
//  HexGrid.m
//  sol
//
//  Created by David Perez on 1/21/13.
//
//

#import "HexGrid.h"

@implementation HexGrid
-(id) initWithDimensionsRows:(int)rows andColumns:(int)columns
{
    if((self = [super init]))
    {
        grid = [NSMutableArray array];
        for (int i = 0; i<rows; i++) {
            NSMutableArray* column = [NSMutableArray array];
            for(int j = 0;j<columns;j++)
            {
                NSObject* t = [NSObject new];
                [column addObject:t];
            }
            [grid addObject:column];
        }
    }
    return self;
}

-(id) init
{
    if((self = [super init]))
    {
        
    }
    return self;
}

-(id) getObjectAtRow:(int)row andColumn:(int)column
{
    int actualRow = row - floor(column/2);
    return [[grid objectAtIndex:actualRow] objectAtIndex: column];
}

-(void) setObject:(NSObject*)t atRow:(int)row andColumn:(int)column
{
    int actualRow = row - floor(column/2);
    [[grid objectAtIndex:actualRow] setObject:t atIndex:column];
}

@end
