//
//  GoalTile.h
//  sol
//
//  Created by David Perez on 1/24/13.
//
//

#import "Tile.h"

@interface GoalTile : Tile


@property (nonatomic) bool satisfied;
@property (nonatomic) int desiredColorNumber;

@end
