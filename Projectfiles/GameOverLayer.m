//
//  GameOverLayer.m
//  sol
//
//  Created by David Perez on 1/24/13.
//
//

#import "GameOverLayer.h"

@implementation GameOverLayer
-(id) init
{
	if ((self = [super init]))
	{
        CCSprite *sprite = [CCSprite spriteWithFile:@"ChromaBall.png"];
        sprite.anchorPoint = CGPointZero;
        sprite.scale = .5;
        [self addChild:sprite z:-1];
        
        CCMenuItemImage *menuItem1 = [CCMenuItemImage itemFromNormalImage:@"PlayButton.png"
                                                            selectedImage: @"PlayButton.png"
                                                                   target:self
                                                                 selector:@selector(doSomething:)];
        
        menuItem1.tag = 1; //this is the property we can access in our doSomething method
        CCMenu *myMenu = [CCMenu menuWithItems:menuItem1, nil];
        
        myMenu.position = CGPointMake(240.0f, 80.0f);
        [self addChild: myMenu];
    }
    return self;
}

-(void) doSomething:(CCMenuItem  *) menuItem
{
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFlipY transitionWithDuration:1 scene:[GameLayer scene:@"levelBlank"]]];
}
+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameOverLayer *layer = [GameOverLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

@end
