//
//  CreditsLayer.m
//  sol
//
//  Created by David Perez on 2/1/13.
//
//

#import "CreditsLayer.h"

@implementation CreditsLayer
-(id) init
{
	if ((self = [super init]))
	{
        CCSprite *sprite = [CCSprite spriteWithFile:@"credits.png"];
        sprite.anchorPoint = CGPointZero;
        [self addChild:sprite z:-1];
        
        CCMenuItemImage *menuItem1 = [CCMenuItemImage itemFromNormalImage:@"BackButton.png"
                                                            selectedImage: @"BackButton.png"
                                                                   target:self
                                                                 selector:@selector(doSomething:)];
        
        menuItem1.tag = 1; //this is the property we can access in our doSomething method
        CCMenu *myMenu = [CCMenu menuWithItems:menuItem1, nil];
        
        myMenu.position = CGPointMake(240.0f, 80.0f);
        [self addChild: myMenu];
    }
    return self;
}

-(void) doSomething:(CCMenuItem  *) menuItem
{
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[MainMenuLayer scene]]];
}
+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	CreditsLayer *layer = [CreditsLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
@end
