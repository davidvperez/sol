//
//  PathPoint.m
//  sol
//
//  Created by David Perez on 1/25/13.
//
//

#import "PathPoint.h"

@implementation PathPoint

@synthesize thisTag;
@synthesize nextTags;

-(id) init
{
    if((self=[super init]))
    {
        nextTags = [NSMutableDictionary dictionaryWithCapacity:0];
    }
    return self;
}

@end
