//
//  MainMenuLayer.h
//  sol
//
//  Created by David Perez on 2/1/13.
//
//

#import "CCLayer.h"
#import "cocos2d.h"
#import "LevelSelectLayer.h"
#import "CreditsLayer.h"

@interface MainMenuLayer : CCLayer
+(id) scene;

@end
