//
//  MainMenuLayer.m
//  sol
//
//  Created by David Perez on 2/1/13.
//
//

#import "MainMenuLayer.h"

@implementation MainMenuLayer
-(id) init
{
	if ((self = [super init]))
	{
        CCSprite *sprite = [CCSprite spriteWithFile:@"ChromaBall.png"];
        sprite.anchorPoint = CGPointZero;
        sprite.scale = .5;
        [self addChild:sprite z:-1];
        
        CCMenuItemImage *menuItem1 = [CCMenuItemImage itemFromNormalImage:@"PlayButton.png"
                                                            selectedImage: @"PlayButton.png"
                                                                   target:self
                                                                 selector:@selector(doSomething:)];
        menuItem1.tag = 1;
        
        CCMenuItemImage *menuItem2 = [CCMenuItemImage itemFromNormalImage:@"CreditsButton.png"
                                                            selectedImage: @"CreditsButton.png"
                                                                   target:self
                                                                 selector:@selector(doSomething:)];
        menuItem2.tag = 2;
        
        CCMenu *myMenu = [CCMenu menuWithItems:menuItem1, menuItem2, nil];
        
        myMenu.position = CGPointZero;
        
        menuItem1.position = ccp(110.0, 130.0);
        menuItem2.position = ccp(250.0, 80.0);
        
        [self addChild: myMenu];
    }
    return self;
}

-(void) doSomething:(CCMenuItem  *) menuItem
{
    switch(menuItem.tag)
    {
        case 1:
        {
            [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[LevelSelectLayer scene]]];
            break;
        }
        case 2:
        {
            [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[CreditsLayer scene]]];
            break;
        }
            
    }
}

+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MainMenuLayer *layer = [MainMenuLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

@end
