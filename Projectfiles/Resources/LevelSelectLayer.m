//
//  LevelSelectLayer.m
//  sol
//
//  Created by David Perez on 2/1/13.
//
//

#import "LevelSelectLayer.h"

@implementation LevelSelectLayer
-(id) init
{
	if ((self = [super init]))
	{
        CCSprite *sprite = [CCSprite spriteWithFile:@"StageSelectionMenu.png"];
        sprite.anchorPoint = CGPointZero;
        sprite.scale = .5;
        [self addChild:sprite z:-1];
    
        CCMenuItemImage *menuItem1 = [CCMenuItemImage itemFromNormalImage:@"RedGoalTile.png"
                                                            selectedImage: @"RedSplatter.png"
                                                                   target:self
                                                                 selector:@selector(doSomething:)];
        menuItem1.tag = 2;
        menuItem1.scale = .3;
        CCMenuItemImage *menuItem2 = [CCMenuItemImage itemFromNormalImage:@"BlueGoalTile.png"
                                                            selectedImage: @"BlueSplatter.png"
                                                                   target:self
                                                                 selector:@selector(doSomething:)];
        menuItem2.tag = 3;
        menuItem2.scale = .3;
        CCMenuItemImage *menuItem3 = [CCMenuItemImage itemFromNormalImage:@"YellowGoalTile.png"
                                                            selectedImage: @"YellowSplatter.png"
                                                                   target:self
                                                                 selector:@selector(doSomething:)];
        menuItem3.tag = 4;
        menuItem3.scale = .3;
        CCMenuItemImage *menuItem4 = [CCMenuItemImage itemFromNormalImage:@"PurpleGoalTile.png"
                                                            selectedImage: @"PurpleSplatter.png"
                                                                   target:self
                                                                 selector:@selector(doSomething:)];
        menuItem4.tag = 5;
        menuItem4.scale = .3;
        CCMenuItemImage *menuItem5 = [CCMenuItemImage itemFromNormalImage:@"OrangeGoalTile.png"
                                                            selectedImage: @"OrangeSplatter.png"
                                                                   target:self
                                                                 selector:@selector(doSomething:)];
        menuItem5.tag = 6;
        menuItem5.scale = .3;
        CCMenuItemImage *menuItem6 = [CCMenuItemImage itemFromNormalImage:@"GreenGoalTile.png"
                                                            selectedImage: @"GreenSplatter.png"
                                                                   target:self
                                                                 selector:@selector(doSomething:)];
        menuItem6.tag = 7;
        menuItem6.scale = .3;
        CCMenuItemImage *backitem = [CCMenuItemImage itemFromNormalImage:@"BackButton.png"
                                                            selectedImage: @"BackButton.png"
                                                                   target:self
                                                                 selector:@selector(doSomething:)];
        backitem.tag = 0;
        backitem.scale = .8;
        CCMenu *myMenu = [CCMenu menuWithItems:menuItem1, menuItem2,menuItem3,menuItem4,menuItem5,menuItem6,backitem, nil];
        
        menuItem1.position = ccp(240, 240);
        menuItem2.position = ccp(70, 80);
        menuItem3.position = ccp(80, 400);
        menuItem4.position = ccp(180, 120);
        menuItem5.position = ccp(210, 380);
        menuItem6.position = ccp(110, 280);
        backitem.position = ccp(270, 60);
        
        myMenu.position = CGPointZero;
        [self addChild: myMenu];
        
        
    }
    return self;
}

-(void) doSomething:(CCMenuItem  *) menuItem
{
    switch(menuItem.tag)
    {
        case 0:
        {
            [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[MainMenuLayer scene]]];
            break;
        }
        case 2:
        {
            [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[GameLayer scene:@"level3"]]];
            break;
        }
        case 3:
        {
            [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[GameLayer scene:@"level5"]]];
            break;
        }
        case 4:
        {
            [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[GameLayer scene:@"level1"]]];
            break;
        }
        case 5:
        {
            [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[GameLayer scene:@"level6"]]];
            break;
        }
        case 6:
        {
            [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[GameLayer scene:@"level2"]]];
            break;
        }
        case 7:
        {
            [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[GameLayer scene:@"level4"]]];
            break;
        }
            
    }
}

+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	LevelSelectLayer *layer = [LevelSelectLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
@end
