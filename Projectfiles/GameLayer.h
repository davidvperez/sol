//
//  GameLayer.h
//  sol
//
//  Created by David Perez on 1/18/13.
//

#import "kobold2d.h"
#import "Tile.h"
#import "Ball.h"
#import "DitchTile.h"
#import "BallStartTile.h"
#import "GoalTile.h"
#import "RedirectTile.h"
#import "PathPoint.h"
#import "MainMenuLayer.h"
#import "LevelSelectLayer.h"
#import "SimpleAudioEngine.h"
#import "GameEndLayer.h"

@interface GameLayer : CCLayer
{
    float CELL_WIDTH;
    int NUM_ROWS, NUM_COLUMNS;
    
    NSMutableDictionary *tiles;
    NSMutableDictionary *backgroundTiles;
    NSMutableSet *validTags;
    
    NSMutableDictionary *expectedPaths;
    NSMutableDictionary *takenPaths;
    
    NSMutableArray *balls;
    
    Tile *tempTile;
    NSNumber *tempTileTag, *tempTilePrevTag;
    
    bool running;
    bool rearranging;
    bool stopped;
    
    CCSprite *currentTrail;
    
    CCMenuItemImage *rollImage;
    CCMenuItemImage *stopImage;
    CCMenuItemImage *resetImage;
    CCMenu *myMenu;
}
+(id) scene:(NSString*)levelString;

@end
