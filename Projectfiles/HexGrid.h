//
//  HexGrid.h
//  sol
//
//  Created by David Perez on 1/21/13.
//
//

#import <Foundation/Foundation.h>

@interface HexGrid : NSObject
{
    NSMutableArray* grid;
}
-(id) initWithDimensionsRows:(int)rows andColumns:(int)columns;

@end
