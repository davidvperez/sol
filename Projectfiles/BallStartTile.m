//
//  BallStartTile.m
//  sol
//
//  Created by David Perez on 1/24/13.
//
//

#import "BallStartTile.h"

@implementation BallStartTile

@synthesize life;
@synthesize ballFileName;
@synthesize direction;

-(id) initWithFile:(NSString *)filename
{
    if((self = [super initWithFile:filename]))
    {
        ballFileName = @"ball.png";
    }
    return self;
}

-(Ball*) createBall
{
    Ball *ball = [Ball spriteWithFile:ballFileName];
    ball.life = life;
    ball.scale = ([self contentSize].width / [ball contentSize].width)*(150.0/400.0)*self.scaleX;
    ball.tag = [self.tags objectAtIndex:0];
    ball.previousTag = [self.tags objectAtIndex:0];
    ball.colorNumber = self.colorNumber;
    return ball;
}

-(void) updatePaths
{
    [super updatePaths];
    PathPoint *pp = [self.pathPoints objectForKey:[self.tags objectAtIndex:0]];
    int currentTagInt = [[self.tags objectAtIndex:0] intValue];
    switch(direction)
    {
        case 0:
            [pp.nextTags setObject:[NSNumber numberWithInt:currentTagInt - [self.tagKey intValue]] forKey:[self.tags objectAtIndex:0]];
            self.rotation = 180.0;
            break;
        case 1:
            [pp.nextTags setObject:[NSNumber numberWithInt:currentTagInt - 1] forKey:[self.tags objectAtIndex:0]];
            self.rotation = 240.0;
            break;
        case 2:
            [pp.nextTags setObject:[NSNumber numberWithInt:currentTagInt + [self.tagKey intValue] - 1] forKey:[self.tags objectAtIndex:0]];
            self.rotation = 300.0;
            break;
        case 3:
            [pp.nextTags setObject:[NSNumber numberWithInt:currentTagInt + [self.tagKey intValue]] forKey:[self.tags objectAtIndex:0]];
            self.rotation = 0.0;
            break;
        case 4:
            [pp.nextTags setObject:[NSNumber numberWithInt:currentTagInt + 1] forKey:[self.tags objectAtIndex:0]];
            self.rotation = 60.0;
            break;
        case 5:
            [pp.nextTags setObject:[NSNumber numberWithInt:currentTagInt - [self.tagKey intValue] + 1] forKey:[self.tags objectAtIndex:0]];
            self.rotation = 120.0;
            break;
    }
    [self.pathPoints setObject:pp forKey:[self.tags objectAtIndex:0]];
}

@end
