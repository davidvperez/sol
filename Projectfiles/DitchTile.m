//
//  DitchTile.m
//  sol
//
//  Created by David Perez on 1/27/13.
//
//

#import "DitchTile.h"

@implementation DitchTile

-(id) initWithFile:(NSString *)filename
{
    if((self = [super initWithFile:filename]))
    {
        
    }
    return self;
}

-(void) updatePaths
{
    [super updatePaths];
    PathPoint *pp = [self.pathPoints objectForKey:[self.tags objectAtIndex:0]];
    int bti = [[self.tags objectAtIndex:0] intValue];
    NSNumber *bt = [NSNumber numberWithInt: bti];
    NSNumber *bt0 = [NSNumber numberWithInt: bti - [self.tagKey intValue]];
    NSNumber *bt1 = [NSNumber numberWithInt: bti - 1];
    NSNumber *bt2 = [NSNumber numberWithInt: bti - 1 + [self.tagKey intValue]];
    NSNumber *bt3 = [NSNumber numberWithInt: bti + [self.tagKey intValue]];
    NSNumber *bt4 = [NSNumber numberWithInt: bti + 1];
    NSNumber *bt5 = [NSNumber numberWithInt: bti - [self.tagKey intValue] + 1];
    [pp.nextTags setObject:bt forKey:bt];
    [pp.nextTags setObject:bt forKey:bt3];
    [pp.nextTags setObject:bt forKey:bt4];
    [pp.nextTags setObject:bt forKey:bt5];
    [pp.nextTags setObject:bt forKey:bt0];
    [pp.nextTags setObject:bt forKey:bt1];
    [pp.nextTags setObject:bt forKey:bt2];
}

@end
