//
//  GameOverLayer.h
//  sol
//
//  Created by David Perez on 1/24/13.
//
//

#import "CCLayer.h"
#import "cocos2d.h"
#import "GameLayer.h"

@interface GameOverLayer : CCLayer
+(id) scene;

@end
