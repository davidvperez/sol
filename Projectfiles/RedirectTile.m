//
//  RedirectTile.m
//  sol
//
//  Created by David Perez on 1/24/13.
//
//

#import "RedirectTile.h"

@implementation RedirectTile

-(id) initWithFile:(NSString *)filename
{
    if((self = [super initWithFile:filename]))
    {
        file = filename;
    }
    return self;
}

-(void) updatePaths
{
    [super updatePaths];
}

@end
