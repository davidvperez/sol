//
//  GameLayer.m
//  sol
//
//  Created by David Perez on 1/18/13.
//
//NOTES:
//red    - 2
//blue   - 3
//yellow - 4
//purple - 5
//orange - 6
//green  - 7

#import "GameLayer.h"

@implementation GameLayer

#define GAME_HEIGHT 440.0
#define GAME_WIDTH 320.0
#define TAG_KEY 100 //(2*(NUM_COLUMNS + 1))
#define BOTTOM_BUFFER 40
-(id) init
{
    if((self = [super init]))
    {
        //initialize gamestates
        running = false;
        rearranging = true;
        
        //audio
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"theme song.mp3" loop:true];
        
        //reset balls
        [self resetBalls];
        
        //schedule update
        [self scheduleUpdate];
        
        rollImage = [CCMenuItemImage itemFromNormalImage:@"RollButton.png"
                                           selectedImage: @"RollButton.png"
                                                  target:self
                                                selector:@selector(doSomething:)];
        rollImage.tag = 1;
        stopImage = [CCMenuItemImage itemFromNormalImage:@"StopButton.png"
                                           selectedImage: @"StopButton.png"
                                                  target:self
                                                selector:@selector(doSomething:)];
        stopImage.tag = 3;
        resetImage = [CCMenuItemImage itemFromNormalImage:@"ResetButton.png"
                                           selectedImage: @"ResetButton.png"
                                                  target:self
                                                selector:@selector(doSomething:)];
        resetImage.tag = 4;
        
        CCMenuItemImage *menuItem2 = [CCMenuItemImage itemFromNormalImage:@"LevelButton.png"
                                                            selectedImage: @"LevelButton.png"
                                                                   target:self
                                                                 selector:@selector(doSomething:)];
        menuItem2.tag = 2;
        
        myMenu = [CCMenu menuWithItems:rollImage, menuItem2, nil];
        
        myMenu.position = CGPointZero;
        
        rollImage.position = ccp(230.0, 20.0);
        resetImage.position = ccp(230.0, 20.0);
        stopImage.position = ccp(230.0, 20.0);
        menuItem2.position = ccp(90.0, 20.0);
        
        rollImage.scale = .25;
        resetImage.scale = .25;
        stopImage.scale = .25;
        menuItem2.scale = .25;
        
        [self addChild: myMenu];
    }
    return self;
}

-(void) doSomething:(CCMenuItem  *) menuItem
{
    switch(menuItem.tag)
    {
        case 1:
        {
            [self updateExpectedPath];
            
            for(Ball *ball in balls)
            {
                BallStartTile *bt = [tiles objectForKey: ball.tag];
                ball.life = bt.life;
            }
            rearranging = false;
            running = true;
            stopped = false;
            
            [myMenu removeChild:rollImage cleanup:false];
            [myMenu addChild:resetImage];

            break;
        }
        case 2:
        {
            [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[LevelSelectLayer scene]]];
            break;
        }
        case 4:
        {
            [self updateExpectedPath];
            [self resetBalls];
            rearranging = true;
            running = false;
            stopped = true;
            
            [myMenu removeChild:resetImage cleanup:false];
            [myMenu addChild:rollImage];
        }
    }
}



//------------------------------------------------------------------------------------------
//HELPER FUNCITONS ------------------------------ UPDATE -----------------------------------
//------------------------------------------------------------------------------------------


-(void) update:(ccTime)dt
{
    KKInput* input = [KKInput sharedInput];
    if([input anyTouchBeganThisFrame])
    {
        if(rearranging)
        {
            CGPoint location = [input anyTouchLocation];
            tempTileTag = [self tagFromPoint:location];
            if([tiles objectForKey:tempTileTag]!=nil)
            {
                if([[tiles objectForKey:tempTileTag] isKindOfClass:[BallStartTile class]])
                {
                    BallStartTile *bt = [tiles objectForKey:tempTileTag];
                    [self rotateBTCC:bt];
                    tempTileTag = nil;
                }
                else
                {
                    tempTile = [self removeTileAtTag:tempTileTag fromLayer:false];
                    if(!tempTile.moveable)
                    {
                        [self addTile:tempTile toLayer:false];
                        tempTile = nil;
                        tempTileTag = nil;
                    }
                }
                tempTilePrevTag = tempTileTag;
            }
            else
                tempTileTag = nil;
        }
        CGPoint location = [input anyTouchLocation];
        if(location.y < BOTTOM_BUFFER)
        {
            if(location.x > GAME_WIDTH/2)
            {
            }
        }
    }
    if([input anyTouchEndedThisFrame])
    {
        if(rearranging)
        {
            if(tempTile != nil)
            {
                //get touch location, and convert to tag form
                CGPoint location = [input anyTouchLocation];
                NSNumber *newTag = [self tagFromPoint:location];
                if([newTag intValue] != [tempTilePrevTag intValue])
                {
                    NSNumber *translation = [NSNumber numberWithInt:([newTag intValue]-[tempTilePrevTag intValue])];
                    [self translateTile:tempTile byTranslation:translation];
                }
                [self addTile:tempTile toLayer:false];
                tempTile = nil;
                tempTileTag = nil;
                tempTilePrevTag = nil;
            }
            [self resetBalls];
        }
    }
    if([input touchesAvailable])
    {
        if(rearranging)
        {
            if(tempTile!=nil)
            {
                CGPoint location = [input anyTouchLocation];
                NSNumber *newTag = [self tagFromPoint:location];
                if([newTag intValue] != [tempTilePrevTag intValue])
                {
                    NSNumber *translation = [NSNumber numberWithInt:([newTag intValue]-[tempTilePrevTag intValue])];
                    if([self translateTile:tempTile byTranslation:translation])
                        tempTilePrevTag = newTag;
                }
                [self updateExpectedPath];
            }
        }
    }
    
    //forall ballspeed = 0 -> game over
    
    if(running)
    {
        [self updateBalls:dt];
        [self drawTakenPath];
        [self checkWin];
    }
    
    if(rearranging)
    {
        [self drawExpectedPath];
    }
}

-(void) updateBalls:(ccTime)dt
{
    for(Ball *ball in balls)
    {
        Tile *ballTile = [tiles objectForKey: ball.tag];
        if ([ballTile isKindOfClass:[GoalTile class]])
        {
            GoalTile *gt = ballTile;
            if(!gt.satisfied && gt.desiredColorNumber == ball.colorNumber)
                ball.life = 0;
        }
        //change location
        if([self pointFromTag:ball.tag].x == ball.position.x && [self pointFromTag:ball.tag].y == ball.position.y)
        {
            if(ball.life > 0)
            {
                if([ball.action isDone])
                {
                    if(ballTile == nil)
                    {
                        Tile *bt = [backgroundTiles objectForKey: ball.tag];
                        if(bt.colorNumber != 1)
                        {
                            if(ball.colorNumber == bt.colorNumber)
                            {
                                ball.colorNumber = bt.colorNumber;
                                [self changeBall:ball toColor:ball.colorNumber];
                            }
                            else if(ball.colorNumber + bt.colorNumber < 8 && ball.colorNumber < 5)
                            {
                                ball.colorNumber = ball.colorNumber+bt.colorNumber;
                                [self changeBall:ball toColor:ball.colorNumber];
                            }
                        }
                        else
                        {
                            ball.colorNumber = 0;
                            [self changeBall:ball toColor:ball.colorNumber];
                        }
                    }
                    
                    
                    ball.life -= 1;
                    PathPoint *pp = [expectedPaths objectForKey:ball.tag];
                    NSNumber *nextTag = [pp.nextTags objectForKey:ball.previousTag];
                    if(nextTag != nil)
                    {
                        ball.action = [CCMoveTo actionWithDuration:.3 position:[self pointFromTag:nextTag]];
                        [ball runAction:ball.action];
                        ball.previousTag = ball.tag;
                        ball.tag = nextTag;
                    }
                }
            }
            if ([ballTile isKindOfClass:[GoalTile class]])
            {
                GoalTile *gt = ballTile;
                if(gt.desiredColorNumber == ball.colorNumber)
                {
                    gt.satisfied = true;
                }
            }
        }
    
    
        //update ball if necessary
        
    }
}

-(void) updateTakenPath
{
    
}

-(void) updateExpectedPath
{
    expectedPaths = [NSMutableDictionary dictionaryWithCapacity:0];
    for(NSNumber *baseTag in validTags)
    {
        if([tiles objectForKey:baseTag]==nil)
        {
            Tile *tile = [backgroundTiles objectForKey:baseTag];
            PathPoint *pp = [tile.pathPoints objectForKey:baseTag];
            [expectedPaths setObject:pp forKey:baseTag];
        }
        else
        {
            Tile *tile = [tiles objectForKey:baseTag];
            PathPoint *pp = [tile.pathPoints objectForKey:baseTag];
            [expectedPaths setObject:pp forKey:baseTag];
        }
    }
}

-(void) resetBalls
{
    
    for(Ball *ball in balls)
    {
        [self removeChild:ball cleanup:true];
    }
    [balls removeAllObjects];
    for(Tile* tile in [tiles allValues])
    {
        if([tile isKindOfClass:[BallStartTile class]])
        {
            BallStartTile *bt = tile;
            Ball *ball = [bt createBall];
            [balls addObject:ball];
            [self addChild:ball z:5];
            ball.position = [self pointFromTag:[tile.tags objectAtIndex:0]];
        }
        if([tile isKindOfClass:[GoalTile class]])
        {
            GoalTile *gt = tile;
            gt.satisfied = false;
        }
    }
}

-(bool) checkWin
{
    for(Tile *tile in [tiles allValues])
    {
        if([tile isKindOfClass:[GoalTile class]])
        {
            GoalTile *gt = tile;
            if(!gt.satisfied)
            {
                return false;
            }
        }
    }
    [self unscheduleUpdate];
    
    [self addChild:[[GameEndLayer alloc] init] z:10];
    //[[CCDirector sharedDirector] replaceScene: [CCTransitionJumpZoom transitionWithDuration:1 scene:[LevelSelectLayer scene]]];
    return true;
}

//------------------------------------------------------------------------------------------
//HELPER FUNCITONS ----------------------------- DRAWING -----------------------------------
//------------------------------------------------------------------------------------------

-(void) drawTakenPath
{
    
}

-(void) drawExpectedPath
{
    
}

//------------------------------------------------------------------------------------------
//HELPER FUNCITONS ------------------------- COORDINATE MAPPING ----------------------------
//------------------------------------------------------------------------------------------

-(NSNumber*) tagFromRow:(int)x andColumn:(int)y
{
    NSNumber *tag = [NSNumber numberWithInt:(x * TAG_KEY) + y];
    return tag;
}

-(int) rowFromTag:(NSNumber*)tag
{
    int col = ([tag intValue]+(1000*TAG_KEY)) % TAG_KEY;
    if(col>TAG_KEY/2)
        return floor([tag floatValue] / TAG_KEY)+1;
    return floor([tag floatValue] / TAG_KEY);
    
}

-(int) columnFromTag:(NSNumber*)tag
{
    int col = ([tag intValue]+(1000*TAG_KEY)) % TAG_KEY;
    if(col>TAG_KEY/2)
        col = col - TAG_KEY;
    return col;
}

-(CGPoint) pointFromTag:(NSNumber*)tag
{
    float x = CELL_WIDTH*(((1.5)*[self columnFromTag:tag])+1);
    float y = (sqrt(3)/2)*CELL_WIDTH*(1+[self columnFromTag:tag]+2*[self rowFromTag:tag]) + BOTTOM_BUFFER;
    return ccp(x,y);
}

-(NSNumber*) tagFromPoint:(CGPoint)point
{
    NSNumber *retTag = @0;
    int column = floor((point.x - CELL_WIDTH)/((3*CELL_WIDTH)/2));
    int row = floor(((point.y - ((sqrt(3)*CELL_WIDTH)/2))/(sqrt(3)*CELL_WIDTH)) - ((point.x - CELL_WIDTH)/((3*CELL_WIDTH)/2)/2));
    NSNumber *taglowerlower = [self tagFromRow:row andColumn:column];
    NSNumber *taglowerupper = [self tagFromRow:row andColumn:column+1];
    NSNumber *tagupperlower = [self tagFromRow:row+1 andColumn:column];
    NSNumber *tagupperupper = [self tagFromRow:row+1 andColumn:column+1];
    NSArray *taglist = [NSArray arrayWithObjects:taglowerlower,taglowerupper,tagupperlower,tagupperupper, nil];
    float minDist = -1;
    for(NSNumber *tag in taglist)
    {
        CGPoint tagLocation = [self pointFromTag:tag];
        float dist = sqrt((tagLocation.x-point.x)*(tagLocation.x-point.x)+(tagLocation.y-point.y)*(tagLocation.y-point.y));
        if(minDist<0 || minDist>dist)
        {
            retTag = tag;
            minDist = dist;
        }
    }
    return retTag;
}


//------------------------------------------------------------------------------------------
//HELPER FUNCITONS ----------------------- TILE MOTION WRAPPERS ----------------------------
//------------------------------------------------------------------------------------------

-(bool) translateTile:(Tile*)tile byTranslation:(NSNumber*)tagValue
{
    [tile translate:tagValue];
    for(NSNumber *tag in tile.tags)
    {
        if(![validTags containsObject:tag])
        {
            [tile translate:[NSNumber numberWithInt:(-[tagValue intValue])]];
            return false;
        }
    }
    for(Tile *otherTile in [tiles allValues])
    {
        if([tile overlapsWithTile:otherTile])
        {
            [tile translate:[NSNumber numberWithInt:(-[tagValue intValue])]];
            return false;
        }
    }
    float x = tile.position.x + [self columnFromTag:tagValue]*(1.5)*(CELL_WIDTH);
    float y = tile.position.y + [self rowFromTag:tagValue]*sqrt(3)*(CELL_WIDTH) + [self columnFromTag:tagValue]*(sqrt(3)/2)*(CELL_WIDTH);
    tile.position = ccp(x,y);
    
    //[tile updatePaths];
    return true;
}

-(bool) rotateBTCC:(BallStartTile*)tile
{
    int bti = [[tile.tags objectAtIndex:0] intValue];
    int bd = tile.direction;
    for(int i = 1; i < 6; i ++)
    {
        int nd = (bd + i) % 6;
        switch(nd)
        {
            case 0:
            {
                if ([validTags containsObject:[NSNumber numberWithInt:(bti-TAG_KEY)]])
                {
                    tile.direction = 0;
                    [tile updatePaths];
                }
                break;
            }
            case 1:
            {
                if ([validTags containsObject:[NSNumber numberWithInt:(bti-1)]])
                {
                    tile.direction = 1;
                    [tile updatePaths];
                }
                break;
            }
            case 2:
            {
                if ([validTags containsObject:[NSNumber numberWithInt:(bti+TAG_KEY-1)]])
                {
                    tile.direction = 2;
                    [tile updatePaths];
                }
                break;
            }
            case 3:
            {
                if ([validTags containsObject:[NSNumber numberWithInt:(bti+TAG_KEY)]])
                {
                    tile.direction = 3;
                    [tile updatePaths];
                }
                break;
            }
            case 4:
            {
                if ([validTags containsObject:[NSNumber numberWithInt:(bti+1)]])
                {
                    tile.direction = 4;
                    [tile updatePaths];
                }
                break;
            }
            case 5:
            {
                if ([validTags containsObject:[NSNumber numberWithInt:(bti-TAG_KEY+1)]])
                {
                    tile.direction = 5;
                    [tile updatePaths];
                }
                break;
            }
        }
    }
    return true;
}

//------------------------------------------------------------------------------------------
//HELPER FUNCITONS ---------------BACKGROUND TILE CREATORS ---------------------------------
//------------------------------------------------------------------------------------------

//background tiles
-(void) addBackgroundTileAtTag:(NSNumber*)tag andString:(NSString*)file
{
    //initialize tile
    Tile *tile = [Tile spriteWithFile:file];
    
    //resize tile
    float rW = (2*CELL_WIDTH)/[tile contentSize].width;
    float rH = (sqrt(3)*CELL_WIDTH)/[tile contentSize].height;
    tile.scaleX = rW;
    tile.scaleY = rH;
    
    //set tagkey
    tile.tagKey = [NSNumber numberWithInt: TAG_KEY];
    
    //relocate tile
    tile.position = [self pointFromTag:tag];
    
    //set tile tag
    [tile.tags setObject:tag atIndexedSubscript:0];
    
    //update paths
    [tile updatePaths];
    
    //add tile to background tileset
    [backgroundTiles setObject:tile forKey:tag];
    
    //add tile to layer
    [self addChild:tile z:-1];
}

//side tiles
-(void) addSideTileAtTag:(NSNumber*)tag withDirectionRight:(bool)facingRight andString:(NSString*)file
{
    //initialize tile
    Tile *tile = [Tile spriteWithFile:file];
    
    //resize tile
    float rW = (2*CELL_WIDTH)/[tile contentSize].width;
    float rH = (sqrt(3)*CELL_WIDTH)/[tile contentSize].height;
    tile.scaleX = rW;
    tile.scaleY = rH;
    
    //set tagkey
    tile.tagKey = [NSNumber numberWithInt: TAG_KEY];
    
    //rotate for right side
    if(facingRight)
        tile.rotation = 180.0;
    
    //relocate tile
    tile.position = [self pointFromTag:tag];
    
    //set tile tag
    [tile.tags setObject:tag atIndexedSubscript:0];
    
    //update paths
    [tile updatePaths];
    
    //setCorrectPathing
    PathPoint *pp = [tile.pathPoints objectForKey:tag];
    int bti = [tag intValue];
    if(facingRight)
    {
        NSNumber *topright = [NSNumber numberWithInt: bti + 1];
        NSNumber *botright = [NSNumber numberWithInt: bti - TAG_KEY + 1];
        [pp.nextTags setObject:topright forKey:botright];
        [pp.nextTags setObject:botright forKey:topright];
    }
    else
    {
        NSNumber *topleft = [NSNumber numberWithInt: bti + TAG_KEY - 1];
        NSNumber *botleft = [NSNumber numberWithInt: bti - 1];
        [pp.nextTags setObject:topleft forKey:botleft];
        [pp.nextTags setObject:botleft forKey:topleft];
    }
    
    //add tile to background tileset
    [backgroundTiles setObject:tile forKey:tag];
    
    //add tile to layer
    [self addChild:tile z:-1];
}

//corner tiles (0 = bot left , 1 = top left, 2 = top right, 3 = bot right)
-(void) addCornerTileAtTag:(NSNumber*)tag withLabel:(int)direction andString:(NSString*)file
{
    //initialize tile
    Tile *tile = [Tile spriteWithFile:file];
    
    //resize tile
    float rW = (2*CELL_WIDTH)/[tile contentSize].width;
    float rH = (sqrt(3)*CELL_WIDTH)/[tile contentSize].height;
    tile.scaleX = rW;
    tile.scaleY = rH;
    
    //set tagkey
    tile.tagKey = [NSNumber numberWithInt: TAG_KEY];
    
    //relocate tile
    tile.position = [self pointFromTag:tag];
    
    //set tile tag
    [tile.tags setObject:tag atIndexedSubscript:0];
    
    //update paths
    [tile updatePaths];
    
    //rotate and set pathing
    PathPoint *pp = [tile.pathPoints objectForKey:tag];
    int bti = [tag intValue];
    switch (direction) {
        case 0:
        {
            tile.rotation = 0.0;
            NSNumber *topright = [NSNumber numberWithInt: bti + 1];
            NSNumber *top = [NSNumber numberWithInt:bti + TAG_KEY];
            [pp.nextTags setObject:topright forKey:top];
            [pp.nextTags setObject:top forKey:topright];
            break;
        }
        case 1:
        {
            tile.rotation = 120.0;
            NSNumber *botright = [NSNumber numberWithInt: bti - TAG_KEY + 1];
            NSNumber *bot = [NSNumber numberWithInt:bti - TAG_KEY];
            [pp.nextTags setObject:botright forKey:bot];
            [pp.nextTags setObject:bot forKey:botright];
            break;
        }
        case 2:
        {
            tile.rotation = 180.0;
            NSNumber *botleft = [NSNumber numberWithInt: bti - 1];
            NSNumber *bot = [NSNumber numberWithInt:bti - TAG_KEY];
            [pp.nextTags setObject:botleft forKey:bot];
            [pp.nextTags setObject:bot forKey:botleft];
            break;
        }
        case 3:
        {
            tile.rotation = 300.0;
            NSNumber *topleft = [NSNumber numberWithInt: bti + TAG_KEY - 1];
            NSNumber *top = [NSNumber numberWithInt:bti + TAG_KEY];
            [pp.nextTags setObject:top forKey:topleft];
            [pp.nextTags setObject:topleft forKey:top];
            break;
        }
        default:
            break;
    }
    
    
    //add tile to background tileset
    [backgroundTiles setObject:tile forKey:tag];
    
    //add tile to layer
    [self addChild:tile z:-1];
}

//top tiles
-(void) addTopTileAtTag:(NSNumber*)tag withDirectionDown:(bool)facingDown andInner:(bool)inner andString:(NSString*)file
{
    //initialize tile
    
    Tile *tile = [Tile spriteWithFile:file];
    
    //resize tile
    float rW = (2*CELL_WIDTH)/[tile contentSize].width;
    float rH = (sqrt(3)*CELL_WIDTH)/[tile contentSize].height;
    tile.scaleX = rW;
    tile.scaleY = rH;
    
    //set tagkey
    tile.tagKey = [NSNumber numberWithInt: TAG_KEY];
    
    //rotate for right side
    if(facingDown)
        tile.rotation = 180.0;
    
    //relocate tile
    tile.position = [self pointFromTag:tag];
    
    //set tile tag
    [tile.tags setObject:tag atIndexedSubscript:0];
    
    //update paths
    [tile updatePaths];
    
    //setCorrectPathing
    PathPoint *pp = [tile.pathPoints objectForKey:tag];
    int bti = [tag intValue];
    if(facingDown)
    {
        if(!inner)
        {
            NSNumber *botleft = [NSNumber numberWithInt: bti - 1];
            NSNumber *botright = [NSNumber numberWithInt: bti - TAG_KEY + 1];
            [pp.nextTags setObject:botleft forKey:botright];
            [pp.nextTags setObject:botright forKey:botleft];
        }
        NSNumber *bot = [NSNumber numberWithInt: bti - TAG_KEY];
        [pp.nextTags setObject:bot forKey:bot];
    }
    else
    {
        if(!inner)
        {
            NSNumber *topright = [NSNumber numberWithInt: bti + 1];
            NSNumber *topleft = [NSNumber numberWithInt: bti + TAG_KEY - 1];
            [pp.nextTags setObject:topleft forKey:topright];
            [pp.nextTags setObject:topright forKey:topleft];
        }
        NSNumber *top = [NSNumber numberWithInt: bti + TAG_KEY];
        [pp.nextTags setObject:top forKey:top];
    }
    
    //add tile to background tileset
    [backgroundTiles setObject:tile forKey:tag];
    
    //add tile to layer
    [self addChild:tile z:-1];
}

//color tiles
-(void) addColorTileAtTag:(NSNumber*)tag withFileString:(NSString*)file andColor:(int)color
{
    //initialize tile
    Tile *tile = [backgroundTiles objectForKey:tag];
    CCSprite *splash = [CCSprite spriteWithFile:file];
    
    //resize tile and splash
    float rW = (2*CELL_WIDTH)/[tile contentSize].width;
    float rH = (sqrt(3)*CELL_WIDTH)/[tile contentSize].height;
    splash.scaleX = rW;
    splash.scaleY = rH;
    
    //set color
    tile.colorNumber = color;
    
    //relocate splash
    splash.position = [self pointFromTag:tag];
    
    //add tile to layer
    [self addChild:splash z:4];
}

//------------------------------------------------------------------------------------------
//HELPER FUNCITONS ---------------- DRAW SPLATTER TRAILS ---------------------------------
//------------------------------------------------------------------------------------------

-(void)drawSplatterTrailFrom:(CGPoint)from to:(CGPoint)to withColor:(int)colorNumber
{
    [self removeChild:currentTrail cleanup:true];
    NSString *file;
    switch(colorNumber)
    {
        case 2:
        {
            file = @"RTrail.png";
        }
        case 3:
        {
            file = @"BTrail.png";
        }
        case 4:
        {
            file = @"YTrail.png";
        }
        case 5:
        {
            file = @"PTrail.png";
        }
        case 6:
        {
            file = @"OTrail.png";
        }
        case 7:
        {
            file = @"GTrail.png";
        }
    }
    currentTrail = [CCSprite spriteWithFile:file];
    
}


-(void)changeBall:(Ball*)ball toColor:(int)color
{
    switch(color)
    {
        case 0:
        {
            [ball setColor:ccWHITE];
            break;
        }
        case 2:
        {
            [ball setColor:ccc3(185, 20, 0)];
            break;
        }
        case 3:
        {
            [ball setColor:ccBLUE];
            break;
        }
        case 4:
        {
            [ball setColor:ccYELLOW];
            break;
        }
        case 5:
        {
            [ball setColor:ccc3(135, 40, 184)];
            break;
        }
        case 6:
        {
            [ball setColor:ccORANGE];
            break;
        }
        case 7:
        {
            [ball setColor:ccGREEN];
            break;
        }
    }
    
}

//------------------------------------------------------------------------------------------
//HELPER FUNCITONS ---------------- MECHANIC TILE CREATORS ---------------------------------
//------------------------------------------------------------------------------------------


//start tile
-(void) addStartTileAtTag:(NSNumber*)tag withFileString:(NSString*)file andDirection:(int) direction andLife:(int)life andBallFile:(NSString*)ballFile
{
    //initialize tile
    BallStartTile *tile = [BallStartTile spriteWithFile:file];
    
    //initialize ball name
    tile.ballFileName = ballFile;
    
    //initialize moveable
    tile.moveable = false;
    
    //initialize tagkey
    tile.tagKey = [NSNumber numberWithInt:TAG_KEY];
    
    //add tags to taglist
    [tile.tags setObject:tag atIndexedSubscript:0];
    
    //initialize life and direction
    tile.life = life;
    tile.direction = direction;
    
    //updatePaths
    [tile updatePaths];
    
    //resize tile
    float rW = (2*CELL_WIDTH)/[tile contentSize].width;
    float rH = (sqrt(3)*CELL_WIDTH)/[tile contentSize].height;
    tile.scaleX = rW;
    tile.scaleY = rH;
    
    //relocate tile
    tile.position = [self pointFromTag:tag];
    
    
    //add tile to data storage and to layer
    [tiles setObject:tile forKey:tag];
    [self addChild:tile z:1];
}


//ditch tile
-(void) addDitchTileAtTag:(NSNumber*)tag withFileString:(NSString*)file
{
    //initialize tile
    DitchTile *tile = [DitchTile spriteWithFile:file];
    
    //initialize moveable
    tile.moveable = false;
    
    //initialize tagkey
    tile.tagKey = [NSNumber numberWithInt:TAG_KEY];
    
    //add tags to taglist
    [tile.tags setObject:tag atIndexedSubscript:0];
    
    //updatePaths
    [tile updatePaths];
    
    //resize tile
    float rW = (2*CELL_WIDTH)/[tile contentSize].width;
    float rH = (sqrt(3)*CELL_WIDTH)/[tile contentSize].height;
    tile.scaleX = rW;
    tile.scaleY = rH;
    
    //relocate tile
    tile.position = [self pointFromTag:tag];
    
    //add tile to data storage and to layer
    [tiles setObject:tile forKey:tag];
    [self addChild:tile z:1];
}

//goal tile
-(void) addGoalTileAtTag:(NSNumber*)tag withFileString:(NSString*)file andColor:(int)color
{
    //initialize tile
    GoalTile *tile = [GoalTile spriteWithFile:file];
    
    //initialize moveable
    tile.moveable = false;
    
    //initialize color
    tile.desiredColorNumber = color;
    
    //initialize tagkey
    tile.tagKey = [NSNumber numberWithInt:TAG_KEY];
    
    //create taglist
    [tile.tags setObject:tag atIndexedSubscript:0];
    
    //updatePaths
    [tile updatePaths];
    
    //resize tile
    float rW = (2*CELL_WIDTH)/[tile contentSize].width;
    float rH = (sqrt(3)*CELL_WIDTH)/[tile contentSize].height;
    tile.scaleX = rW;
    tile.scaleY = rH;
    
    //relocate tile
    tile.position = [self pointFromTag:tag];
    
    //add tile to data storage and to layer
    [tiles setObject:tile forKey:tag];
    [self addChild:tile z:1];
}

-(void) addRedirectTileAtTag:(NSMutableArray*)tags withFileString:(NSString*)file andMoveable:(bool) moveable andSizeType:(int)sizeType andPathPointData:(NSDictionary*)ppData
{
    //initialize tile
    Tile *tile = [RedirectTile spriteWithFile:file];
    
    //initialize moveable
    tile.moveable = moveable;
    
    //initialize tagkey
    tile.tagKey = [NSNumber numberWithInt:TAG_KEY];
    
    //create taglist
    tile.tags = tags;
    
    //start pathing
    [tile updatePaths];
    
    //edit pathing
    for (NSNumber *tag in tags) {
        PathPoint *pp = [tile.pathPoints objectForKey:tag];
        if(pp == nil)
        {
            pp = [[PathPoint alloc] init];
            pp.thisTag = tag;
            [tile.pathPoints setObject:pp forKey:tag];
        }
        NSArray *data = [ppData objectForKey:[tag stringValue]];
        int bti = [tag intValue];
        [pp.nextTags setObject:[data objectAtIndex:0] forKey:[NSNumber numberWithInt:bti - TAG_KEY]];
        [pp.nextTags setObject:[data objectAtIndex:1] forKey:[NSNumber numberWithInt:bti - 1]];
        [pp.nextTags setObject:[data objectAtIndex:2] forKey:[NSNumber numberWithInt:bti + TAG_KEY - 1]];
        [pp.nextTags setObject:[data objectAtIndex:3] forKey:[NSNumber numberWithInt:bti + TAG_KEY]];
        [pp.nextTags setObject:[data objectAtIndex:4] forKey:[NSNumber numberWithInt:bti + 1]];
        [pp.nextTags setObject:[data objectAtIndex:5] forKey:[NSNumber numberWithInt:bti - TAG_KEY + 1]];
        [pp.nextTags setObject:[data objectAtIndex:6] forKey:tag];
    }
    
    switch(sizeType)
    {
        case 1:
        {
            //resize tile
            float rW = (2*CELL_WIDTH)/[tile contentSize].width;
            float rH = (sqrt(3)*CELL_WIDTH)/[tile contentSize].height;
            tile.scaleX = rW;
            tile.scaleY = rH;
            
            //relocate tile
            tile.position = [self pointFromTag:[tags objectAtIndex:0]];
            break;
        }
        case 2:
        {
            //resize tile
            float rW = (2*CELL_WIDTH)/[tile contentSize].width;
            float rH = 2*(sqrt(3)*CELL_WIDTH)/[tile contentSize].height;
            tile.scaleX = rW;
            tile.scaleY = rH;
            
            //relocate tile
            float x1 = [self pointFromTag:[tags objectAtIndex:0]].x;
            float x2 = [self pointFromTag:[tags objectAtIndex:1]].x;
            float y1 = [self pointFromTag:[tags objectAtIndex:0]].y;
            float y2 = [self pointFromTag:[tags objectAtIndex:1]].y;
            tile.position = ccp((x1+x2)/2.0,(y1+y2)/2.0);
            break;
        }
        case 3:
        {
            //resize tile
            float rW = (3.5*CELL_WIDTH)/[tile contentSize].width;
            float rH = 1.5*(sqrt(3)*CELL_WIDTH)/[tile contentSize].height;
            tile.scaleX = rW;
            tile.scaleY = rH;
            
            //relocate tile
            float x1 = [self pointFromTag:[tags objectAtIndex:0]].x;
            float x2 = [self pointFromTag:[tags objectAtIndex:1]].x;
            float y1 = [self pointFromTag:[tags objectAtIndex:0]].y;
            float y2 = [self pointFromTag:[tags objectAtIndex:1]].y;
            tile.position = ccp((x1+x2)/2.0,(y1+y2)/2.0);
            break;
        }
    }
    
    //add tile to data storage and to layer
    for (NSNumber *tag in tags) {
        [tiles setObject:tile forKey:tag];
    }
    [self addChild:tile z:5];
}


//tile addition / removal
-(Tile*) removeTileAtTag:(NSNumber*)removeTag fromLayer:(bool)removal
{
    //get the tile to be removed
    Tile *removeTile = [tiles objectForKey:removeTag];
    //remove the tile from the dictionary, for each tag associated with the tile
    [tiles removeObjectsForKeys:removeTile.tags];
    if(removal)
        [self removeChild:removeTile cleanup:false];
    return removeTile;
}

-(bool) addTile:(Tile*)tile toLayer:(bool)addal
{
    for(NSNumber *tag in tile.tags)
    {
        if([tiles objectForKey:tag]!=nil)
            return false;
    }
    for(NSNumber *tag in tile.tags)
    {
        [tiles setObject:tile forKey:tag];
    }
    if(addal)
    {
        [self addChild:tile];
    }
    //returns true on successful adding of a tile to the tileset
    return true;
}



//------------------------------------------------------------------------------------------
//HELPER FUNCITONS ----------------------- STARTUP METHODS ---------------------------------
//------------------------------------------------------------------------------------------


+(id) scene:(NSString*)levelString
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameLayer *layer = [GameLayer node];
    
    //setup level
    [layer setupLevel:levelString];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(void) setupLevel:(NSString*)levelString
{
    
    //Startup Property Lists
    NSString *path = [[NSBundle mainBundle] pathForResource:@"levels" ofType:@"plist"];
    NSDictionary *plist = [NSDictionary dictionaryWithContentsOfFile:path];
    NSDictionary *levelData = [plist objectForKey:levelString];
    
    //initialize constants
    NSNumber *num = [levelData objectForKey:@"cellSize"];
    CELL_WIDTH = [num floatValue]; //5x6 - 40, 7x9 - 28
    NUM_ROWS = (int)((GAME_HEIGHT / CELL_WIDTH)/sqrt(3));
    NUM_COLUMNS = (int)(((GAME_WIDTH - (CELL_WIDTH/2))/ CELL_WIDTH)/1.5);
    
    //initialize sets
    backgroundTiles = [NSMutableDictionary dictionaryWithCapacity:0];
    tiles = [NSMutableDictionary dictionaryWithCapacity:0];
    validTags = [NSMutableSet setWithCapacity:0];
    balls = [NSMutableArray arrayWithCapacity:0];
    
    //initialize pathsets
    expectedPaths = [NSMutableDictionary dictionaryWithCapacity:0];
    takenPaths = [NSMutableDictionary dictionaryWithCapacity:0];
    
    //determine which tag locations are valid
    for(int row = 0; row < NUM_ROWS; row++)
    {
        for(int column = 0; column < NUM_COLUMNS; column++)
        {
            int tRow = row - column/2;
            NSNumber *tag = [self tagFromRow:tRow andColumn:column];
            int topY = [self pointFromTag:tag].y + (sqrt(3)*CELL_WIDTH/2);
            if(topY<=480)
                [validTags addObject:tag];
        }
    }
    
    //create background tiles in valid tag locations
    for(NSNumber *tag in validTags)
    {
        int bti = [tag intValue];
        if(bti % TAG_KEY == 0)
        {
            if(bti/TAG_KEY == 0)
            {
                [self addCornerTileAtTag:tag withLabel:0 andString:[levelData objectForKey:@"cornerFile"]];
            }
            else if(bti/TAG_KEY == NUM_ROWS-1)
            {
                [self addCornerTileAtTag:tag withLabel:1 andString:[levelData objectForKey:@"cornerFile"]];
            }
            else
            {
                [self addSideTileAtTag:tag withDirectionRight:true andString:[levelData objectForKey:@"sideFile"]];
            }
        }
        else if((bti + TAG_KEY*1000) % TAG_KEY == NUM_COLUMNS - 1)
        {
            if(bti/TAG_KEY + NUM_COLUMNS/2 == 1)
            {
                [self addCornerTileAtTag:tag withLabel:3 andString:[levelData objectForKey:@"cornerFile"]];
            }
            else if(bti/TAG_KEY + NUM_COLUMNS/2 == NUM_ROWS-1)
            {
                [self addCornerTileAtTag:tag withLabel:2 andString:[levelData objectForKey:@"cornerFile"]];
            }
            else
            {
                [self addSideTileAtTag:tag withDirectionRight:false andString:[levelData objectForKey:@"sideFile"]];
            }
        }
        else if(![validTags containsObject:[NSNumber numberWithInt:bti+TAG_KEY]])
        {
            if(![validTags containsObject:[NSNumber numberWithInt:bti+1]])
            {
                [self addTopTileAtTag:tag withDirectionDown:true andInner:false andString:[levelData objectForKey:@"topOuterFile"]];
            }
            else
            {
                [self addTopTileAtTag:tag withDirectionDown:true andInner:true andString:[levelData objectForKey:@"topInnerFile"]];
            }
        }
        else if(![validTags containsObject:[NSNumber numberWithInt:bti-TAG_KEY]])
        {
            if(![validTags containsObject:[NSNumber numberWithInt:bti-1]])
            {
                [self addTopTileAtTag:tag withDirectionDown:false andInner:false andString:[levelData objectForKey:@"topOuterFile"]];
            }
            else
            {
                [self addTopTileAtTag:tag withDirectionDown:false andInner:true andString:[levelData objectForKey:@"topInnerFile"]];
            }
        }
        else
        {
            [self addBackgroundTileAtTag:tag andString:[levelData objectForKey:@"backgroundFile"]];
        }
    }
    
    
    //add splashed of color!
    //add red splashes
    for(NSString *tagS in [levelData objectForKey:@"redSplashTiles"])
    {
        NSNumber *tag = [NSNumber numberWithInt:[tagS integerValue]];
        [self addColorTileAtTag:tag withFileString:[levelData objectForKey:@"splashRedFile"] andColor:2];
    }
    //add blue splashes
    for(NSString *tagS in [levelData objectForKey:@"blueSplashTiles"])
    {
        NSNumber *tag = [NSNumber numberWithInt:[tagS integerValue]];
        [self addColorTileAtTag:tag withFileString:[levelData objectForKey:@"splashBlueFile"] andColor:3];
    }
    //add yellow splashes
    for(NSString *tagS in [levelData objectForKey:@"yellowSplashTiles"])
    {
        NSNumber *tag = [NSNumber numberWithInt:[tagS integerValue]];
        [self addColorTileAtTag:tag withFileString:[levelData objectForKey:@"splashYellowFile"] andColor:4];
    }
    //add purple splashes
    for(NSString *tagS in [levelData objectForKey:@"purpleSplashTiles"])
    {
        NSNumber *tag = [NSNumber numberWithInt:[tagS integerValue]];
        [self addColorTileAtTag:tag withFileString:[levelData objectForKey:@"splashPurpleFile"] andColor:5];
    }
    //add orange splashes
    for(NSString *tagS in [levelData objectForKey:@"orangeSplashTiles"])
    {
        NSNumber *tag = [NSNumber numberWithInt:[tagS integerValue]];
        [self addColorTileAtTag:tag withFileString:[levelData objectForKey:@"splashOrangeFile"] andColor:6];
    }
    //add green splashes
    for(NSString *tagS in [levelData objectForKey:@"greenSplashTiles"])
    {
        NSNumber *tag = [NSNumber numberWithInt:[tagS integerValue]];
        [self addColorTileAtTag:tag withFileString:[levelData objectForKey:@"splashGreenFile"] andColor:7];
    }
    
    //add water splashes
    for(NSString *tagS in [levelData objectForKey:@"WaterSplashTiles"])
    {
        NSNumber *tag = [NSNumber numberWithInt:[tagS integerValue]];
        [self addColorTileAtTag:tag withFileString:[levelData objectForKey:@"splashWaterFile"] andColor:1];
    }
    
    //add black hole tiles
    for(NSString *tagS in [levelData objectForKey:@"blackHoleTiles"])
    {
        NSNumber *tag = [NSNumber numberWithInt:[tagS integerValue]];
        [self addDitchTileAtTag:tag withFileString:[levelData objectForKey:@"blackHoleFile"]];
    }
    
    //add goal tiles
    NSDictionary *goalTiles = [levelData objectForKey:@"goalTiles"];
    for(NSNumber *tagS in [goalTiles allKeys])
    {
        NSNumber *tag = [NSNumber numberWithInt:[tagS integerValue]];
        switch([[goalTiles objectForKey:tagS] intValue])
        {
            case 2:
            {
                [self addGoalTileAtTag:tag withFileString:[levelData objectForKey:@"goalRedFile"] andColor:2];
                break;
            }
            case 3:
            {
                [self addGoalTileAtTag:tag withFileString:[levelData objectForKey:@"goalBlueFile"] andColor:3];
                break;
            }
            case 4:
            {
                [self addGoalTileAtTag:tag withFileString:[levelData objectForKey:@"goalYellowFile"] andColor:4];
                break;
            }
            case 5:
            {
                [self addGoalTileAtTag:tag withFileString:[levelData objectForKey:@"goalPurpleFile"] andColor:5];
                break;
            }
            case 6:
            {
                [self addGoalTileAtTag:tag withFileString:[levelData objectForKey:@"goalOrangeFile"] andColor:6];
                break;
            }
            case 7:
            {
                [self addGoalTileAtTag:tag withFileString:[levelData objectForKey:@"goalGreenFile"] andColor:7];
                break;
            }
        }
    }
    
    //add start tiles
    NSDictionary *startTiles = [levelData objectForKey:@"startTiles"];
    for(NSString *tagS in [startTiles allKeys])
    {
        NSNumber *tag = [NSNumber numberWithInt:[tagS integerValue]];
        [self addStartTileAtTag:tag withFileString:[levelData objectForKey:@"startTileFile"] andDirection:[[[startTiles objectForKey:tagS] objectAtIndex:0] intValue] andLife:[[[startTiles objectForKey:tagS] objectAtIndex:1] intValue] andBallFile:[levelData objectForKey:@"ballFile"]];
    }
    
    //add redirection tiles
    NSArray *redirectionTiles = [levelData objectForKey:@"redirectionTiles"];
    for(NSDictionary *tileData in redirectionTiles)
    {
        [self addRedirectTileAtTag:[tileData objectForKey:@"tags"] withFileString:[tileData objectForKey:@"fileName"] andMoveable:[tileData objectForKey:@"moveable"] andSizeType:[[tileData objectForKey:@"sizeType"] intValue] andPathPointData:[tileData objectForKey:@"pathPoints"]];
    }
    
    [self updateExpectedPath];
    [self resetBalls];
}

@end
