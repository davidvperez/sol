//
//  GoalTile.m
//  sol
//
//  Created by David Perez on 1/24/13.
//
//

#import "GoalTile.h"

@implementation GoalTile

@synthesize satisfied;
@synthesize desiredColorNumber;

-(id) init
{
    if((self = [super init]))
    {
        satisfied = false;
        desiredColorNumber = 0;
    }
    return self;
}

@end
