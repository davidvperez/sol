//
//  DitchTile.h
//  sol
//
//  Created by David Perez on 1/27/13.
//
//

#import "Tile.h"

@interface DitchTile : Tile

-(void) updatePaths;

@end
