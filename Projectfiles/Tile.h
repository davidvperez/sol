//
//  Tile.h
//  sol
//
//  Created by David Perez on 1/21/13.
//
//

#import "CCSprite.h"
#import "PathPoint.h"

@interface Tile : CCSprite
{
	
}

@property (nonatomic, copy) NSMutableArray* tags;
@property (nonatomic, copy) NSNumber* tagKey;
@property (nonatomic) bool moveable;
@property (nonatomic) int colorNumber;
@property (nonatomic,copy) NSMutableDictionary *pathPoints;

-(bool) overlapsWithTile:(Tile*)otherTile;
-(void) translate:(NSNumber*)tagValue;
-(void) rotateCCWAtTag:(NSNumber*)pivotTag;
-(void) updatePaths;


@end