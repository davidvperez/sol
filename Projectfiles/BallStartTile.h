//
//  BallStartTile.h
//  sol
//
//  Created by David Perez on 1/24/13.
//
//

#import "Tile.h"
#import "Ball.h"

@interface BallStartTile : Tile
{
    
}
@property (nonatomic) int life;
@property (nonatomic) int direction;
@property (nonatomic, copy) NSString *ballFileName;

-(Ball*) createBall;
-(void) updatePaths;

@end
