//
//  Ball.m
//  sol
//
//  Created by David Perez on 1/24/13.
//
//

#import "Ball.h"

@implementation Ball

@synthesize tag;
@synthesize colorNumber;
@synthesize previousTag;
@synthesize life;
@synthesize action;

-(id) initWithFile:(NSString *)filename
{
    if((self = [super initWithFile:filename]))
    {
        action = [[CCAction alloc] init];
        colorNumber = 0;
    }
    return self;
}

@end
