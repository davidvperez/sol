//
//  Vector.h
//  sol
//
//  Created by David Perez on 1/23/13.
//
//

#import <Foundation/Foundation.h>

@interface Vector : NSObject

@property (nonatomic) int speed;
@property (nonatomic) int direction;

@end
