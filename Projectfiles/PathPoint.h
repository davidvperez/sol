//
//  PathPoint.h
//  sol
//
//  Created by David Perez on 1/25/13.
//
//

#import "kobold2d.h"

@interface PathPoint : NSObject

@property (nonatomic, copy) NSNumber *thisTag;
@property (nonatomic, copy) NSMutableDictionary *nextTags;

@end
