//
//  Vector.m
//  sol
//
//  Created by David Perez on 1/23/13.
//
//

#import "Vector.h"

@implementation Vector

@synthesize speed;
@synthesize direction;


-(id) init
{
    if((self = [super init]))
    {
        speed = 0;
        direction = 0;
    }
    return self;
}
@end
