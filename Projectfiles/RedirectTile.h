//
//  RedirectTile.h
//  sol
//
//  Created by David Perez on 1/24/13.
//
//

#import "Tile.h"

@interface RedirectTile : Tile
{
    NSString *file;
}
-(void) updatePaths;
@end
