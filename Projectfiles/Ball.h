//
//  Ball.h
//  sol
//
//  Created by David Perez on 1/24/13.
//
//

#import "kobold2d.h"

@interface Ball : CCSprite

@property (nonatomic, copy) NSNumber *tag;
@property (nonatomic) int life;
@property (nonatomic) int colorNumber;
@property (nonatomic, copy) NSNumber *previousTag;
@property (nonatomic, copy) CCAction *action;

@end
