//
//  Tile.m
//  sol
//
//  Created by David Perez on 1/21/13.
//
//

#import "Tile.h"

@implementation Tile

@synthesize tags;
@synthesize colorNumber;
@synthesize tagKey;
@synthesize moveable;
@synthesize pathPoints;

-(id) initWithFile:(NSString *)filename
{
    if((self = [super initWithFile:filename]))
    {
        moveable = true;
        pathPoints = [[NSMutableDictionary alloc] init];
        tags = [[NSMutableArray alloc] init];
        [tags setObject:@0 atIndexedSubscript:0];
        colorNumber = 0;
    }
    return self;
}

-(void) rotateCCWAtTag:(NSNumber*)pivotTag
{
    NSMutableArray* newTags = [NSMutableArray arrayWithCapacity:0];
    for(NSNumber* tag in tags)
    {
        NSNumber* newTag = [NSNumber numberWithInt:(-floor([tag intValue]/[tagKey intValue])+[tagKey intValue]*(floor([tag intValue]/[tagKey intValue])+[tag intValue]%[tagKey intValue]))];
        [newTags addObject:newTag];
    }
    tags = newTags;
}

-(void) translate:(NSNumber*)tagValue
{
    NSMutableArray* newTags = [NSMutableArray arrayWithCapacity:0];
    for(NSNumber* tag in tags)
    {
        NSNumber* newTag = [NSNumber numberWithInt:([tag intValue]+[tagValue intValue])];
        [newTags addObject:newTag];
    }
    tags = newTags;
    
    //translate the pathpoints:
    for(PathPoint *pp in [self.pathPoints allValues])
    {
        NSMutableDictionary *newPaths = [[NSMutableDictionary alloc] init];
        NSArray *fromTags = [pp.nextTags allKeys];
        for (NSNumber *fromTag in fromTags) {
            NSNumber *toTag = [pp.nextTags objectForKey:fromTag];
            int fti = [fromTag intValue];
            int tti = [toTag intValue];
            [newPaths setObject:[NSNumber numberWithInt:tti+[tagValue intValue]] forKey:[NSNumber numberWithInt:fti+[tagValue intValue]]];
        }
        pp.nextTags = newPaths;
        NSNumber *nextTag = [NSNumber numberWithInt:[pp.thisTag intValue]+[tagValue intValue]];
        pp.thisTag = nextTag;
    }
    NSMutableDictionary *newPoints = [[NSMutableDictionary alloc] init];
    for(NSNumber *tag in [self.pathPoints allKeys])
    {
        PathPoint *pp = [self.pathPoints objectForKey:tag];
        NSNumber *newTag = [NSNumber numberWithInt:[tag intValue]+[tagValue intValue]];
        [newPoints setObject:pp forKey:newTag];
    }
    self.pathPoints = newPoints;
}

-(bool) overlapsWithTile:(Tile*)otherTile
{
    for (NSNumber* otherTag in otherTile.tags)
    {
        for (NSNumber* tag in tags)
        {
            if([tag intValue] == [otherTag intValue])
                return true;
        }
    }
    return false;
}

-(void) updatePaths
{
    PathPoint *pp = [[PathPoint alloc] init];
    pp.thisTag = [self.tags objectAtIndex:0];
    int bti = [[self.tags objectAtIndex:0] intValue];
    NSNumber *bt0 = [NSNumber numberWithInt: bti - [self.tagKey intValue]];
    NSNumber *bt1 = [NSNumber numberWithInt: bti - 1];
    NSNumber *bt2 = [NSNumber numberWithInt: bti - 1 + [self.tagKey intValue]];
    NSNumber *bt3 = [NSNumber numberWithInt: bti + [self.tagKey intValue]];
    NSNumber *bt4 = [NSNumber numberWithInt: bti + 1];
    NSNumber *bt5 = [NSNumber numberWithInt: bti - [self.tagKey intValue] + 1];
    [pp.nextTags setObject:bt0 forKey:bt3];
    [pp.nextTags setObject:bt1 forKey:bt4];
    [pp.nextTags setObject:bt2 forKey:bt5];
    [pp.nextTags setObject:bt3 forKey:bt0];
    [pp.nextTags setObject:bt4 forKey:bt1];
    [pp.nextTags setObject:bt5 forKey:bt2];
    [self.pathPoints setObject:pp forKey:[self.tags objectAtIndex:0]];
}


@end
