//
//  GameEndLayer.m
//  sol
//
//  Created by David Perez on 2/1/13.
//
//

#import "GameEndLayer.h"

@implementation GameEndLayer
-(id) init
{
	if ((self = [super init]))
	{
        CCMenuItemImage *menuItem1 = [CCMenuItemImage itemFromNormalImage:@"SuccesPopup.png"
                                                            selectedImage: @"SuccesPopup.png"
                                                                   target:self
                                                                 selector:@selector(doSomething:)];
        menuItem1.tag = 1;
        CCMenu *myMenu = [CCMenu menuWithItems:menuItem1, nil];
        
        myMenu.position = CGPointZero;
        
        menuItem1.scale = .3;
        menuItem1.position = ccp(160,240);
        
        [self addChild: myMenu];
    }
    return self;
}

-(void) doSomething:(CCMenuItem  *) menuItem
{
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[LevelSelectLayer scene]]];
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
}

@end
